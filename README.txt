This is an implementation of Coron homomorphic encryption implemented by Luan Cardoso dos Santos and Guilherme Rodrigues Bilar.
Was implemented at the code a webservice by Lucas Zanco Ladeira that can request homomorphic services.

Environment:
- Python 3.4.2
- gmpy2-2.0.6
- psycopg2-2.6.1