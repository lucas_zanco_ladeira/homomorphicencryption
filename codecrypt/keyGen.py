from gmpy2 import mpz
import random
import gmpy2
import time
import math
from codecrypt import parameters, fheKey

P=parameters.par()
P.setPar(tipo='small')

def mod(z,p, modo = 'mpz'):

    z=mpz(z)
    p=mpz(p)
    mod = z - mpz(z/p)*p
    if modo == 'mpz': return mod
    else: return int(mod)

def genZi(eta):

    eta=mpz(eta)
    seed=int(time.time()*1000000)
    state=gmpy2.random_state(seed)
    while True:
        p=gmpy2.mpz_rrandomb(state, eta)
        if gmpy2.is_odd(p): return p

def genPrime(b):

    while True:
        seed=int(time.time()*1000000)
        state=gmpy2.random_state(seed)
        prime=gmpy2.mpz_rrandomb(state,b)
        prime=gmpy2.next_prime(prime)
        if len(prime)==b:
            return prime


def genX0(p, _gamma, _lambda):

    teto = gmpy2.f_div(2**mpz(_gamma), p)
    while True:
        q0=genPrime(_lambda**2)*genPrime(_lambda**2)
        if q0 < teto: break
    x0 = gmpy2.mul(p, q0)

    return q0, x0

def qrand(q0):
    return random.randint(0, q0)

def rrand(rho):
    limit=2**rho
    return random.randint(-limit, limit)


def genX(beta, rho, p, q0):

    x = list()
    for i in range(beta*2):
        result = gmpy2.mul(p, qrand(q0))
        result = gmpy2.add(result, rrand(rho))
        x.append(result)

    return x

def genSk_(theta, thetam):

    l = math.ceil(math.sqrt(theta))
    s0 = [1]+[0]*(l-1)
    s1 = [1]+[0]*(l-1)
    k = range(1,int(math.sqrt(thetam))+1)
    k1 = random.sample(k, 2)
    k2 = random.sample(k, 3)

    k1.sort()
    k2.sort()
    B = math.floor(math.sqrt(theta/thetam))

    for k in k1:
        index = random.randint(k*B+1,(k+1)*B)-1
        s0[index] = 1
    for k in k2:
        index = random.randint(k*B+1,(k+1)*B)-1
        if index > len(s1)-1: index=random.randint(len(s1)-4, len(s1)-1)
        s1[index] = 1

    return s0, s1

def genSk(theta, thetam):

    l = math.ceil(math.sqrt(theta))
    s0 = [1]+[0]*(l-1)
    s1 = [1]+[0]*(l-1)
    k = range(0, 4)
    ks0 = random.sample(k, 2)
    ks1 = random.sample(k, 4)

    B=math.floor(math.sqrt(theta/thetam))
    for k in ks0:
        idx=random.randint((k*B)+1, (k+1)*B)
        s0[idx-1]=1
    for k in ks1:
        idx=random.randint((k*B)+1, (k+1)*B)
        s1[idx-1]=1

    return s0, s1

def randomMatrix(i, j, kappa, se, sqrtTheta):

    if i==0 and j==0: return 0
    random.seed(se)
    itera = i*sqrtTheta+j
    for i in range(itera):
        a=random.getrandbits(kappa+1)
    return a

def genU11(se, s0, s1, theta, kappa, p):

    si, sj = list(), list()
    for i in range(len(s0)):
        if s0[i]==1: si.append(i)
    for j in range(len(s1)):
        if s1[j]==1: sj.append(j)

    l = math.ceil(math.sqrt(theta))
    mx = 2**mpz(kappa+1)

    xp = gmpy2.div(2**kappa,p)
    xp = mpz(gmpy2.round_away(xp))
    soma = xp%mx
    somatorio = 0
    for i in si:
        for j in sj:
            somatorio += randomMatrix(i, j, kappa, se, l)
    u = soma-somatorio

    return u

def encryptVector(s, p, q0, x0, rho):

    sigma=list()
    p2=2**rho
    for i in s:
        r = random.randint(-p2, p2)
        q = random.randint(0, q0-1)
        c = mod((i+2*r+p*q),x0)
        sigma.append(c)
    return sigma

def keygen(size='small'):

    P.setPar(size)
    p = genZi(P._eta)
    q0, x0 = genX0(p, P._gamma, P._lambda)
    listaX = genX(P._beta, P._rho, p, q0)
    pkAsk = listaX
    pkAsk.insert(0, x0)

    while True:
        s0,s1 = genSk(P._theta, P._thetam)
        if (s0.count(1)*s1.count(1)==15): break

    se = int(time.time()*1000)
    _kappa = P._gamma+6
    u11 = genU11(se, s0, s1, P._theta, _kappa, p)

    sigma0 = encryptVector(s0, p, q0, x0, P._rho)
    sigma1 = encryptVector(s1, p, q0, x0, P._rho)

    public = fheKey.pk(pkAsk, se, u11, sigma0, sigma1, P)
    secret = fheKey.sk(s0, s1, P)

    return (public, secret)

