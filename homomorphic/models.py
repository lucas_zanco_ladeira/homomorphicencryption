from django.db import models

class Message(models.Model):
    name = models.CharField(max_length = 50,null = False)
    email = models.CharField(max_length = 100,null = False)

    pklamba = models.TextField(null = True)
    pkbeta = models.TextField(null = True)
    pkrho = models.TextField(null = True)
    pkask = models.TextField(null = True)
    sks0 = models.TextField(null = True)
    sks1 = models.TextField(null = True)
    sktetha = models.TextField(null = True)
    u11 = models.TextField(null = True)
    sigma0 = models.TextField(null = True)
    sigma1 = models.TextField(null = True)
    P = models.TextField(null = True)

    message = models.TextField(null = True)
    messagecrypt = models.TextField(null = True)
    messageexpand = models.TextField(null = True)
