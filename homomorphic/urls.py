from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^keygen/$', views.keygen, name="keygen"),
    url(r'^encrypt/$', views.encrypt, name="encrypt"),
    url(r'^decrypt/$', views.decrypt, name="decrypt"),
    url(r'^message/$', views.message, name="message"),
    url(r'^$', views.index, name='index'),
]